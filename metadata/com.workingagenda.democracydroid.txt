Categories:Multimedia,Reading,Science & Education
License:GPL-3.0
Web Site:
Source Code:https://github.com/polypmer/DemocracyDroid
Issue Tracker:https://github.com/polypmer/DemocracyDroid/issues
Donate:https://www.democracynow.org/donate

Auto Name:Democracy Droid
Summary:Watch "Democracy Now The war and peace report"
Description:
Democracy Now! is an US-based national, daily, independent, award-winning news
program hosted by journalists Amy Goodman and Juan Gonzalez. Pioneering the
largest public media collaboration in the U.S.

This application streams or downloads the independent media broadcast Democracy
Now! The War and Peace Report. It supports streaming and downloading the audio
and video broadcast - this includes watching and listening to individual
stories, reading transcripts, and streaming the live stream.

If you like the app, you are encouraged to donate to Democracy Now! at their
website.

The app is not affiliated with the production of the show, merely the
distribution.
.

Repo Type:git
Repo:https://github.com/polypmer/DemocracyDroid

Build:2.8.6,20
    commit=f769c7cbec7094252b4ad16ce3cb9190f63e2c98
    subdir=app
    gradle=yes
    rm=app/libs
    prebuild=sed -i "s/files.\(.\)libs.jsoup-\(.*\).jar../\1org.jsoup:jsoup:\2\1/" build.gradle

Build:2.8.8,22
    commit=2304ae32c187ecd9e3f4f6ea0571b1b1785012bf
    subdir=app
    gradle=yes
    rm=app/libs
    prebuild=sed -i "s/files.\(.\)libs.jsoup-\(.*\).jar../\1org.jsoup:jsoup:\2\1/" build.gradle

Build:2.8.9,23
    commit=eeb63de21cb2dbd2db4067e0879e1ebe172b1ab5
    subdir=app
    gradle=yes
    rm=app/libs
    prebuild=sed -i "s/files.\(.\)libs.jsoup-\(.*\).jar../\1org.jsoup:jsoup:\2\1/" build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.8.9
Current Version Code:23
